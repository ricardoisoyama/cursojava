package br.com.cursojava.exercicio.banco;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class Conta {

	private Integer numeroConta;
	private Integer numeroDigito;
	
	public List lancamentos;
	
	public Conta() {
		super();
		this.lancamentos = new ArrayList<>();
	}
	
	public Conta(Integer numeroConta, Integer numeroDigito) {
		super();
		this.numeroConta = numeroConta;
		this.numeroDigito = numeroDigito;
		this.lancamentos = new ArrayList<>();
	}

	public abstract BigDecimal getTaxaRendimento();
	
	public abstract BigDecimal getIR();
	
	public abstract void validarLancamento(Lancamento lancamento) throws SaldoInsuficienteException;
	
	public void realizarLancamento(BigDecimal valor, Date dataLancamento) throws SaldoInsuficienteException {
		Lancamento lancamento = new Lancamento(dataLancamento, valor);
		validarLancamento(lancamento);
		lancamentos.add(lancamento);

		System.out.println(getSaldo());
	}
	
	
	/**
	 * Saldo a partir de um lancamento
	 * lancamento * (1 + taxaRendimento ^ (diferenca Datas))
	 * decrescido do IR
	 * 
	 * @return saldo atual
	 */
	public BigDecimal getSaldo() {
		BigDecimal res = BigDecimal.ZERO;
		
		for (Object object : lancamentos) {
			Lancamento lancamento = (Lancamento) object;
			res = res.add(lancamento.getValorAtualizado(this.getTaxaRendimento(), this.getIR()));
		}
		
		return res;
	}

	public Integer getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(Integer numeroConta) {
		this.numeroConta = numeroConta;
	}

	public Integer getNumeroDigito() {
		return numeroDigito;
	}

	public void setNumeroDigito(Integer numeroDigito) {
		this.numeroDigito = numeroDigito;
	}

	public List getLancamentos() {
		return lancamentos;
	}

	public void setLancamentos(List lancamentos) {
		this.lancamentos = lancamentos;
	}
	
	
	
	
}