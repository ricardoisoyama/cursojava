package br.com.cursojava.exercicio.banco;

public class SaldoInsuficienteException extends BancoException {
	
	public SaldoInsuficienteException(String message) {
		super(message);
	}
	
}
