package br.com.cursojava.comandos.controle;

public class TesteSwitchCaseInteiro {

	
	/**
	 * Metodo que verifica o valor inteiro e retorna ele mesmo
	 * @param enumeravel
	 * @return
	 */
	private static Integer getIntegerValue(Integer inteiro) {
		Integer retorno = null;
		
		
		switch (inteiro) {
		case 10:
			retorno = 10;
			break;
		case 20:
			retorno = 20;
			break;
		case 30:
			retorno = 30;
			break;
		case 40:
			retorno = 40;
			break;
		default:
			retorno = 50;
		}
		
		return retorno;
		
	}
	
	public static void main(String[] args) {
		System.out.println(getIntegerValue(10));
		System.out.println(getIntegerValue(20));
		System.out.println(getIntegerValue(30));
		System.out.println(getIntegerValue(40));
		System.out.println(getIntegerValue(50));
	}
	
}
