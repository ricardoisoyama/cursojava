package br.com.cursojava.exemplo.classe;

public class ExemploClasseComAtributos {
	
	//Modificador default
	String cadeiaDeCaracteres;
	
	//Modificador public
	public Integer umNumero;
	
	//Modificador private, s� dispon�vel dentro da mesma classe(main)
	private Long umNumeroLong;
	
	public static void main(String[] args) {
		ExemploClasseComAtributos ExemploClasseComAtributos = new ExemploClasseComAtributos();
		ExemploClasseComAtributos.umNumero = 1;
		ExemploClasseComAtributos.umNumeroLong = 11l;
	}
}
