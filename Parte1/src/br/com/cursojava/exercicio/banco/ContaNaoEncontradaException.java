package br.com.cursojava.exercicio.banco;

public class ContaNaoEncontradaException extends BancoException {

	public ContaNaoEncontradaException(String message) {
		super(message);
	}
	
}
