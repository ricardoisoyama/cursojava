package br.com.cursojava.exercicio.banco;

public class ContaBloqueadaException extends BancoException {

	public ContaBloqueadaException(String message) {
		super(message);
	}
	
}
