package br.com.cursojava.exercicio.banco;

public class ClienteNaoEncontradoException extends BancoException {

	public ClienteNaoEncontradoException(String message) {
		super(message);
	}
	
}
