package br.com.cursojava.operadoresmatematicos;

public class TesteOperadoresBoolean {

	public static void main(String[] args) {
		
		boolean x, y, z, resultado;
		
		x = true;
		y = false;
		z = x;
		
		int a = 0,
			b = 1;
		
		
		// && and
		resultado = x && y;
		System.out.println(x && y);
		System.out.println(resultado);
		
		// || or
		resultado = x || y;
		System.out.println(x || y);
		System.out.println(resultado);
		
		// ! not
		resultado = !z;
		System.out.println(!y);
		System.out.println(resultado);
		
		// & and (faz as duas comparações)
		System.out.println(a == b & ++a <= b);
		System.out.println(a);
		
		a = 0;
		b = 1;
		// | or (faz comparações)
		System.out.println(a == --b | ++a <= b);
		System.out.println(a);
		
		
	}
	
}
