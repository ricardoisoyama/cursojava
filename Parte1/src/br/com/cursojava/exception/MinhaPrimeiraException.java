package br.com.cursojava.exception;

public class MinhaPrimeiraException extends Exception {

	private String message;
	
	public MinhaPrimeiraException(String message) {
		super();
		this.message = message;
	}
	
	public MinhaPrimeiraException() {
		super();
		
	}
	
	public String getMessage() {
		return message;
	}
}
