package br.com.cursojava.atributos.metodos.classe;

public class ExemploMetodoClasse {
	
	public static Integer potencia2(Integer base) {
		return base * base;
	}
	
	
	public static void main(String[] args) {
		System.out.println(ExemploMetodoClasse.potencia2(4));
		
	}


	// ALT + SHIFT + S, atalhos eclipse
	@Override
	public String toString() {
		return "ExemploMetodoClasse []";
	}
	
	
	
	
	

}
