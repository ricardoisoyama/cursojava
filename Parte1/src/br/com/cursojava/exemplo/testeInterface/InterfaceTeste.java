package br.com.cursojava.exemplo.testeInterface;

public interface InterfaceTeste {
	
	public void metodoA();
	
	public String metodoB();
	
	public Integer[] metodoC();
	
}
