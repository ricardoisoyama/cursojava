package br.com.cursojava.exemplo.classe;

import br.com.cursojava.exemplo.classe.abstrata.ExemploDeClasseAbstrata;

public class ExemploClasseConcreta {
	
	//Classe concreta
	public static void main(String[] args) {
		ExemploClasseComAtributos ExemploClasseComAtributos = new ExemploClasseComAtributos();
		ExemploClasseComAtributos.cadeiaDeCaracteres = "";
		ExemploClasseComAtributos.umNumero = 10;
		
		//ExemploDeClasseAbstrata classeAbstrata = new ExemploDeClasseAbstrata();
		//Classe abstrata n�o pode ser instanciada
	}
	
}
