package br.com.cursojava.exercicio.banco;

import java.math.BigDecimal;

public class ContaPoupanca extends Conta {

	@Override
	public BigDecimal getTaxaRendimento() {
		return new BigDecimal("0.006687");
	}

	@Override
	public BigDecimal getIR() {
		return new BigDecimal("0.15");
	}

	@Override
	public void validarLancamento(Lancamento lancamento) throws SaldoInsuficienteException {
		Boolean res = getSaldo().add(lancamento.getValor()).compareTo(BigDecimal.ZERO) >= 0;
		//return res;
		
		if (!res) {
			
			throw new SaldoInsuficienteException("Conta poupan�a com saldo insuficiente!");
		}
	}

}