package br.com.cursojava.comandos.controle;

public class TesteSwitchCaseString {

	
	/**
	 * Metodo que verifica o valor String e retorna ele mesmo
	 * @param enumeravel
	 * @return
	 */
	private static String getStringValue(String caracter) {
		String retorno = null;
		
		
		switch (caracter) {
		case "A":
			retorno = "A";
			break;
		case "B":
			retorno = "B";
			break;
		case "C":
			retorno = "C";
			break;
		case "D":
			retorno = "D";
			break;
		default:
			retorno = "Valor n�o encontrado";
		}
		
		return retorno;
		
	}
	
	public static void main(String[] args) {
		System.out.println(getStringValue("A"));
		System.out.println(getStringValue("B"));
		System.out.println(getStringValue("C"));
		System.out.println(getStringValue("D"));
		System.out.println(getStringValue("E"));
		
	}
	
}
