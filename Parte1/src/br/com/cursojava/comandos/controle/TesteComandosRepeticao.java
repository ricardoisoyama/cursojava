package br.com.cursojava.comandos.controle;

public class TesteComandosRepeticao {

	public static void main(String[] args) {
		//Teste for
		//Soma das pot�ncias de 2 de 0 at� 99
		Long resultado = 0l;
		
		for (int i = 0; i < 100; i++) {
			resultado += i*i;
		}
		System.out.println("Resultado: " + resultado);
		
		//Teste for com array
		//Escrever primeira linha e depois embaixo escrever fore + ctrl space
		String[] arrayStrings = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};
		
		for (String atual : arrayStrings) {
			System.out.println(atual);
		}
		
		//Teste While
		int contador = 0;
		boolean variavelVerificacao = true;
		while(variavelVerificacao) {
			if (contador++ == 200) {
				variavelVerificacao = false;
			}
			
		}
		System.out.println(contador);
		
		//Teste while sem a variavel, utilizando o comando break(N�O FAZER)
		contador = 0;
		while(true) {
			if (contador++ == 200) {
				break;
			}
			
		}
		
		System.out.println(contador);
		
		//Teste do While
		contador = 0;
		boolean variavelVerificacaoDoWhile = false;
		do {
			contador++;
		} while (variavelVerificacaoDoWhile);
			System.out.println(contador);
		
		//Teste for, resultados exibidos ser�o multiplos de 5, sen�o continua... testando comando continue
		resultado = 0l;
		for (int i = 0; i < 100; i++) {
			if (i%5 == 0) {
				resultado += i*i;
			} else {
				continue;
			}
			System.out.println("Resultado: " + resultado);
		}
		
			
		
	}
	
}
