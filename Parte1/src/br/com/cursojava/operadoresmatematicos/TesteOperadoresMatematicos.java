package br.com.cursojava.operadoresmatematicos;

public class TesteOperadoresMatematicos {

	public static void main(String[] args) {
		
		long x, y, z;
		
		x = 0;
		y = 1;
		z = 3;
			
		// +
		System.out.println(x + y);
		
		System.out.println(x++);
		
		System.out.println(++x);
		
		System.out.println(z--);
		
		System.out.println(--z);
		
	}
	
}
