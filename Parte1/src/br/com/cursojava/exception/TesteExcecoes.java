package br.com.cursojava.exception;

public class TesteExcecoes {

	/**
	 * Exemplo de coment�rio para lan�amento de exce��es
	 * @param inteiro
	 * @throws MinhaPrimeiraException
	 */
	public static void testaHierarquiaDaPrimeiraException(Integer inteiro) throws MinhaPrimeiraException {
		//Se for multiplo de 5
		if (inteiro % 5 == 0) {
			throw new MinhaPrimeiraException();
		}
		
		if (inteiro % 9 == 0) {
			throw new MinhaTerceiraException();
		}
		
	}
	
	/**
	 * Exemplo de coment�rio para lan�amento de exce��es
	 * @param inteiro
	 * @throws MinhaPrimeiraException
	 * @throws MinhaSegundaException 
	 */
	public static void testaTodasException(Integer inteiro) throws MinhaPrimeiraException, MinhaSegundaException {
		//Se for multiplo de 5
		if (inteiro % 5 == 0) {
			throw new MinhaPrimeiraException();
		}
		
		if (inteiro % 9 == 0) {
			throw new MinhaTerceiraException();
		}
		
		if (inteiro % 13 == 0) {
			throw new MinhaSegundaException();
		}
		
		
		
	}
	
	
	
	
	public static void main(String[] args) {
		
// Bloco para teste da minha primeira exception
//		try {
//			testaPrimeiraException(2);
//			testaPrimeiraException(15);
//		} catch (MinhaPrimeiraException e) {
//			System.out.println("Ocorreram erros no processamento.");
//			//Printa no console o log da linha onde ocorreu o erro
//			e.printStackTrace();
//		}
		
		try {
			testaHierarquiaDaPrimeiraException(16);
			testaHierarquiaDaPrimeiraException(5);
		} catch (MinhaPrimeiraException e) {
			System.out.println(e.getClass().toString());
			e.printStackTrace();
		}
	
		try {
			testaHierarquiaDaPrimeiraException(27);
		} catch (MinhaPrimeiraException e) {
			System.out.println(e.getClass().toString());
			e.printStackTrace();
		}
		
		try {
			testaTodasException(5);
		} catch (MinhaPrimeiraException | MinhaSegundaException e) { //Espec�fico do JAVA 7
			System.out.println(e.getClass().toString());
			e.printStackTrace();
		}
		
		try {
			testaTodasException(52);
		} catch (MinhaPrimeiraException e) {
			System.out.println(e.getClass().toString());
			e.printStackTrace();
		} catch (MinhaSegundaException e) {
			System.out.println(e.getClass().toString());
			e.printStackTrace();
		}
		
		
		try {
			testaTodasException(28);
		} catch (Throwable e) { //N�o usar throwable direto, s� para exemplo
			System.out.println(e.getClass().toString());
			e.printStackTrace();
		} finally {
			System.out.println("Bloco finally sempre � executado");
			//return; s� com return para a execu��o do c�digo
		}
	

	}
	
	
}
