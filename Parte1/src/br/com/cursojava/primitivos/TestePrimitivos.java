package br.com.cursojava.primitivos;

public class TestePrimitivos {

	public static void main(String[] args) {
		int primitivoInteiro = 5;
		int primitivoInteiroSemValor = 0;
		
		System.out.println(primitivoInteiro);
		System.out.println(primitivoInteiroSemValor);
		
		char primitivoCaractere = 'a';
		System.out.println(primitivoCaractere);
		
		boolean primitivoBooleano = false;
		System.out.println(primitivoBooleano);
		
		long primitivoLong = 2;
		System.out.println(primitivoLong);
		
		Integer wrapperInteiro = 2;
		System.out.println(wrapperInteiro);
		
		Character wrapperCaractere = 'a';
		System.out.println(wrapperCaractere);
		
		Boolean wrapperBooleano = false;
		System.out.println(wrapperBooleano);
		
		Long wrapperLong = 20l;
		System.out.println(wrapperLong);
		
		System.out.println(wrapperInteiro.getClass());
		
		String[] arrayStrings = new String[10];
		arrayStrings[0] = "Curso Java";
		System.out.println(arrayStrings[0]);
		System.out.println(arrayStrings[1]);
		System.out.println(arrayStrings);
		System.out.println(arrayStrings.toString());
		String stringDiferente="diferente";
		String stringIgual = "diferente";
		
		boolean testeIgualObjetos = stringDiferente == stringIgual;
	
		
		
	}
	
	
}
